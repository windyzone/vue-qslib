import BaseOption from './BaseOption';
import GUID from './GUID';
import DataDefine, { QuestionDataTypes,QuestionTypes } from './QuestionDataTypes';
import OrderBy, {OrderByTypes} from './OrderByTypes';
import MatrixRowTitle from './MatrixRowTitle';
/**
 * 基本题目类定义
 */
export default class BaseQuestionSubject<T> {
   // 题目Id
    public Id: string = '';
    // 题目编号
    public CodeNum:string='';
    public Title: string = '标题';
    public SelValue: string = '';
    public MustAnswer: boolean = true;
    public Options: T[] = [];
    public DataType: QuestionDataTypes = QuestionDataTypes.dtText;
    public OrderValue: OrderByTypes = OrderByTypes.obtVertical;
    public QuestionType:QuestionTypes=QuestionTypes.qtRadio;  
    public checkvalue:string[]=[];
    
    // // 行标题
     public RowTitles: MatrixRowTitle[] = [];
     public RowTitleText: String = '';
    
    

    constructor() {
        this.Id = new GUID().toString();
       
        
    }
}
 
