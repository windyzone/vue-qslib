import BaseQuestionSubject from './TQuestionSubject';
import MultiSelOption from './MultiSelOption';
import { OrderByTypes } from './OrderByTypes';

export default class MultiSelSubject extends BaseQuestionSubject<MultiSelOption> {
    public MaxSel: number = 0;
    public MinSel: number = 0;
    public SelectedValues: string[] = [];
    public BlankValue:number=0;


}
