import BaseQuestionSubject from './TQuestionSubject';
import BaseOption from './BaseOption';


export default class SizeCodeOption extends BaseOption
{
    
    SubItems:Array<SizeCodeOption>=[];
    
    
}
export   class SizeCodeSubject extends BaseQuestionSubject<SizeCodeOption>{
    
    CheckValue="";
}